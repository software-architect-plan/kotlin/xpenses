package com.cursosandroid.xpenses

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class XpensesApp: Application() {
}